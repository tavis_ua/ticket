<?php
/**
 * Created by PhpStorm.
 * User: tavis
 * Date: 31.10.2019
 * Time: 19:23
 */
require_once ('Reservator.php');
require_once ('ReservatorApiPartner.php');


//Заполняю массив событий
$events = [];
for($i = 0; $i<5; $i++){
    $events[] = rand(10, 99);
}

foreach ($events as $eventID){
    if($eventID%2 != 0){
        $className = 'ReservatorApiPartner';
    }else{
        $className = 'Reservator';
    }
    $event = new $className();
    $event->eventId = $eventID;
    $event->reserveRandomTicket();
    echo "<h5>--------------------------------------------------</h5><br>";
}