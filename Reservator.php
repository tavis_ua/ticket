<?php

/**
 * Created by PhpStorm.
 * User: tavis
 * Date: 31.10.2019
 * Time: 21:38
 */
class Reservator
{
    public $prefix = 'local';
    public $eventId;    //ID события
    private $ticketId;  //ID билета
    private $orderId;   //ID заказа

    /*Резервирование билета
     * */
    public function reserveRandomTicket(){
        $this->ticketId = $this->getRandomNumber();
        $this->orderId = $this->getRandomNumber();
        echo '['.$this->prefix.'] | creating object for event #'.$this->eventId.'<br>';
        echo '['.$this->prefix.'] | reserving ticket #'.$this->ticketId.'<br>';
        //Если покупка выполнена через партнерку
        if($this->prefix != 'local') {
            echo '[' . $this->prefix . '] | reserving ticket #' . $this->ticketId . ' VIA PARTNER API CALL<br>';
        }
        echo '['.$this->prefix.'] | orderId #'.$this->orderId.' created<br>';
        echo '['.$this->prefix.'] | Sending admin notification: Order#'.$this->orderId.' created for event #'.$this->eventId.'<br>';
    }

    /*Возвращаю случайное число
     * */
    function getRandomNumber(){
        return rand(99,3000);
    }
}